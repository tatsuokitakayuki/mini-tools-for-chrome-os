chrome.storage.local.get(['key']).then(async result => {
  const text = result.key;
  console.log(text);
  const copyArea = document.createElement('textarea');
  document.body.appendChild(copyArea);
  copyArea.textContent = text;
  copyArea.select();
  document.execCommand('copy');
  document.body.removeChild(copyArea);
  await chrome.storage.local.set({key: ''});
});
