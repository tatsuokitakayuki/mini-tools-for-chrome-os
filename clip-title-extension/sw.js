const CLIP_TITLE_AND_URL_FULL_VERSION = '3.0.202304.0';
const DELIMITER_STRING = ' ';

const onClickedA = async tab => {
  if (tab) {
    const text = tab.title + DELIMITER_STRING + tab.url;
    chrome.storage.local.set({key: text});
    chrome.scripting.executeScript({target: {tabId: tab.id}, files: ['ww.js']});
  }
};

chrome.action.onClicked.addListener(async tab => await onClickedA(tab));
