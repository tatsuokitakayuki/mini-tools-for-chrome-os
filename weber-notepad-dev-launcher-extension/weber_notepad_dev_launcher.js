const WEBER_NOTEPAD_DEV_LAUNCHER_FULL_VERSION = '1.0.202009.1';
const CREATE_DATA = {
    "type": "normal",
    "url": "https://oed.kirari.dev/"
};

const createAppWindow = createData => {
    chrome.windows.create(createData);
};

chrome.browserAction.onClicked.addListener(() => createAppWindow(CREATE_DATA));
